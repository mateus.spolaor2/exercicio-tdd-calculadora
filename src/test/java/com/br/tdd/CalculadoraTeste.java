package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    Calculadora calculadora = new Calculadora();

    @Test
    public void testaSomaDeDoisNumeros(){

        double resultado = calculadora.soma(2,2);
        Assertions.assertEquals(resultado, 4);

    }

    @Test
    public void testaSomaDeDoisNumerosFlutuantes(){

        double resultado = calculadora.soma(3.9,0.1);
        Assertions.assertEquals(resultado, 4);

    }

    @Test
    public void testaSubtracaoDeDoisNumeros(){
        double resultado = calculadora.subtracao(3,1);
        Assertions.assertEquals(resultado, 2);
    }

    @Test
    public void testaSubtracaoDeDoisNumerosDecimais(){
        double resultado = calculadora.subtracao(2.1,0.1);
        Assertions.assertEquals(resultado, 2);
    }

    @Test
    public void testaMultiplicacaoDeDoisInteiros(){
        int resultado = calculadora.multiplicacao(2,2);
        Assertions.assertEquals(resultado, 4);
    }

    @Test
    public void testaMultiplicacaoDeDoisFlutuantes(){
        double resultado = calculadora.multiplicacao(2.5,2.5);
        Assertions.assertEquals(resultado, 6.25);
    }

    @Test
    public void testaDivisaoDeDoisInteiros(){
        double resultado = calculadora.divisao(25,4);
        Assertions.assertEquals(resultado, 6.25);
    }

    @Test
    public void testaDivisaoDeDoisFlutuantes(){
        double resultado = calculadora.divisao(7.5,2.4);
        Assertions.assertEquals(resultado, 3.125);
    }

}
