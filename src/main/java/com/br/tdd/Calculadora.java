package com.br.tdd;

public class Calculadora {

    public int soma(int i, int j) {
        int soma = i+j;
        return soma;
    }

    public double soma(double i, double j) {
        double soma = i+j;
        return soma;
    }

    public int subtracao(int i, int j) {
        return i-j;
    }

    public double subtracao(double i, double j) {
        return i-j;
    }

    public int multiplicacao(int i, int j) {
        return i*j;
    }

    public double multiplicacao(double i, double j) {
        return i*j;
    }

    public double divisao(int i, int j) {
        return (double) i/j;
    }

    public double divisao(double i, double j) {
        return i/j;
    }
}
