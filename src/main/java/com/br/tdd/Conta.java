package com.br.tdd;

public class Conta {

    private Cliente cliente;
    private double saldo;

    public Conta(Cliente cliente, double saldo) {
        this.cliente = cliente;
        this.saldo = saldo;
    }

    public Conta() {
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void depositar(double valorDeDeposito) {
        valorDeDeposito = valorDeDeposito - (valorDeDeposito * 0.2);
        this.saldo += valorDeDeposito;
    }

    public void sacar(double valorDeSaque) {
        if(valorDeSaque > this.saldo){
            throw new RuntimeException("Saldo insuficiente");
        } else {
            this.saldo -= valorDeSaque;
        }
    }
}
